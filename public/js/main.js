function init() {
	let instance = M.Tabs.init(document.querySelectorAll('.tabs'));

	up_song();
	up_playback();
}

function play() {
	fetch('/play', {method: 'POST'}).then((res) => {
		if (res.status !== 200) return;
		/*let iz = document.querySelectorAll('#play i');
		for (let i = 0; i < iz.length; ++i) {
			if (iz[i].classList.contains('hide')) {
				iz[i].classList.remove('hide');
			} else {
				iz[i].classList.add('hide');
			}
		}*/
		up_playback();
	});
}
function fwd() {
	fetch('/fwd', {method: 'POST'}).then((res) => {
		if (res.status !== 200) return;
		up_song();
	});
}
function bwd() {
	fetch('/bwd', {method: 'POST'}).then((res) => {
		if (res.status !== 200) return;
		up_song();
	});
}
function up_song() {
	fetch('/song', {method: 'GET'}).then((res) => {
		return res.json();
	}).then((info) => {
		document.getElementById('song').innerText = info.song;
	});
}
function chlist(num) {
	fetch('/list', {
		method: 'POST',
		headers: {"Content-Type": "application/json; charset=utf-8"},
		body: JSON.stringify({num: num})
	}).then((res) => {
		up_song();
	});
}
function up_playback() {
	fetch('/pb', {method: 'GET'}).then((res) => {
		return res.json();
	}).then((info) => {
		let stat = info.stat;
		if (stat === 'playing') {
			document.getElementById('i-pause').classList.remove('hide');
			document.getElementById('i-play').classList.add('hide');
		} else {
			document.getElementById('i-play').classList.remove('hide');
			document.getElementById('i-pause').classList.add('hide');
		}
		return stat;
	});
}
