var express = require('express');
var router = express.Router();

const { exec } = require('child_process');

function strip(msg) {
	return msg.replace(/\n$/gm, '');
}

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});

router.get('/song', function(req, res, next) {
	exec('audtool current-song', (err, stdout, stderr) => {
		if (err) {
			res.status(500).json({err: err, stderr: stderr});
			return;
		}
		res.json({song: strip(stdout)});
	});
});

router.get('/pb', function(req, res, next) {
	exec('audtool playback-status', (err, stdout, stderr) => {
		if (err) {
			res.status(500).json({err: err, stderr: stderr});
			return;
		}
		res.json({stat: strip(stdout)});
	});
});

router.post('/play', function(req, res, next) {
	exec('audtool playback-playpause', (err, stdout, stderr) => {
		if (err) {
			res.status(500).json({err: err, stderr: stderr});
			return;
		}
		res.status(200).json({play: 'ok'});
	});
});

router.post('/fwd', function(req, res, next) {
	exec('audacious --fwd', (err, stdout, stderr) => {
		if (err) {
			res.status(500).json({err: err, stderr: stderr});
			return;
		}
		res.status(200).json({fwd: 'ok'});
	});
});

router.post('/bwd', function(req, res, next) {
	exec('audacious --rew', (err, stdout, stderr) => {
		if (err) {
			res.status(500).json({err: err, stderr: stderr});
			return;
		}
		res.status(200).json({bwd: 'ok'});
	});
});

router.post('/list', function(req, res, next) {
	let num = parseInt(req.body.num);
	exec(`audtool set-current-playlist ${num}`, (err, stdout, stderr) => {
		if (err) {
			res.status(500).json({err: err, stderr: stderr});
			return;
		}
		res.status(200).json({list: 'ok'});
	});
});

module.exports = router;
