all:
	systemctl status sshd > /dev/null || sudo systemctl start sshd
	sudo -v
	sudo lxterminal -e icecast -c /etc/icecast.xml &
	sudo lxterminal -e openvpn --config ~giraff/naboo/misc/vpn/arch.ovpn &
	pavucontrol &
	sleep 3 && darkice -c ~giraff/naboo/misc/audio_streaming/darkice.cfg &
	DEBUG=audacious-remote:* npm start
css:
	sass ./_src/materialize/sass/materialize.scss ./public/stylesheets/materialize.css
run:
	DEBUG=audacious-remote:* npm start
